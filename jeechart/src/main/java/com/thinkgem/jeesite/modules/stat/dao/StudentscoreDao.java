/**
 * There are <a href="https://github.com/thinkgem/jeesite">JeeSite</a> code generation
 */
package com.thinkgem.jeesite.modules.stat.dao;

import org.springframework.stereotype.Repository;

import com.thinkgem.jeesite.common.persistence.BaseDao;
import com.thinkgem.jeesite.common.persistence.Parameter;
import com.thinkgem.jeesite.modules.stat.entity.Studentscore;

/**
 * 学生成绩DAO接口
 * @author zhangfeng
 * @version 2014-09-18
 */
@Repository
public class StudentscoreDao extends BaseDao<Studentscore> {
	
}
