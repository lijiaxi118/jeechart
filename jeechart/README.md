# Jeehart 报表框架

## 框架简介

## 为何选择

1. 使用 [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) 协议，源代码完全开源，无商业限制。
2. 使用目前最主流的J2EE开发框架，简单易学，学习成本低。
3. 数据库无限制，支持MySql、Oracle、SQL Server、H2等数据库。
4. 模块化设计，层次结构清晰。内置一系列企业信息管理的基础功能。
5. 操作权限控制精密细致，对所有管理链接都进行权限验证，可控制到按钮。
6. 数据权限控制精密细致，对指定数据集权限进行过滤，七种数据权限可供选择。
7. 提供基本功能模块的源代码生成器，提高开发效率及质量。
8. 提供常用工具类封装，日志、缓存、验证、字典、组织机构等，常用标签（taglib），获取当前组织机构、字典等数据。
9. 完全兼容目前最流行浏览器（IE6、IE7+、Firefox、Chrome）。
10. 提供目前最流行的Activit流程引擎实例。

## 技术选型

1、Services相关

* Core Framework：Spring Framework 3.2。
* Security Framework：Apache Shiro 1.2。
* Workflow Engine：Activit 5.14。

2、Web相关

* MVC Framework：SpringMVC 3.2。
* Layout Decoration：SiteMesh 2.4。
* JavaScript Library：JQuery 1.9。
* CSS Framework：Twitter Bootstrap 2.3.1。
* JavaScript/CSS Compressor：YUI Compressor 2.4。
* Front Validation：JQuery Validation Plugin 1.11。

3、Database相关

* ORM Framework：Hibernate 4.1 + MyBatis 3.1。
* Connection Pool：Alibaba Druid 1.0。
* Bean Validation：Hibernate Validation 5.0。
* Cache：Ehcache 2.6。

4、Tools 相关

* Commons：Apache Commons
* JSON Mapper：Jackson 2.1
* Bean Mapper：Dozer 5.3
* Office Tools: Apache POI 3.9
* Full-text search：Hibernate Search 4.2（Apache Lucene 3.6）、IK Analyzer 2012_u6中文分词
* Log Manager：Log4j 1.2

## 安全考虑

1. 开发语言：系统采用Java 语言开发，具有卓越的通用性、高效性、平台移植性和安全性。
2. 分层设计：（数据库层，数据访问层，业务逻辑层，展示层）层次清楚，低耦合，各层必须通过接口才能接入并进行参数校验（如：在展示层不可直接操作数据库），保证数据操作的安全。
3. 双重验证：用户表单提交双验证：包括服务器端验证及客户端验证，防止用户通过浏览器恶意修改（如不可写文本域、隐藏变量篡改、上传非法文件等），跳过客户端验证操作数据库。
4. 安全编码：用户表单提交所有数据，在服务器端都进行安全编码，防止用户提交非法脚本及SQL注入获取敏感数据等，确保数据安全。
5. 密码加密：登录用户密码进行SHA1散列加密，此加密方法是不可逆的。保证密文泄露后的安全问题。
6. 强制访问：系统对所有管理端链接都进行用户身份权限验证，防止用户

## 版权声明

本软件使用 [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) 协议，请严格遵照协议内容：

1. 需要给代码的用户一份Apache Licence。
2. 如果你修改了代码，需要在被修改的文件中说明。
3. **在延伸的代码中（修改和有源代码衍生的代码中）需要带有原来代码中的协议，商标，专利声明和其他原来作者规定需要包含的说明。**
4. 如果再发布的产品中包含一个Notice文件，则在Notice文件中需要带有Apache Licence。你可以在Notice中增加自己的许可，但不可以表现为对Apache Licence构成更改。
3. Apache Licence也是对商业应用友好的许可。使用者也可以在需要的时候修改代码来满足需要并作为开源或商业产品发布/销售

